# bing
node wrapper around bing web search api

# Usage

```js
var Bing = require('./');

// EXAMPLE SEARCH WITH BING
var bing = Bing({
  key: '[ENTER API KEY]',
  query: 'colts history',
  maxResults: 60
})

bing
  .search(function(results){
    console.log(results.length)
    console.log(results[0])
  })

```

# Options
The following configuration options are available

- ```key```: API key from [Bing](https://datamarket.azure.com/dataset/bing/search)
- ```query```: Search terms
- ```skip```: Skip results from the top
- ```maxResults```: Maximum results to return
- ```format```: default ```JSON```