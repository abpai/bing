"use strict";

module.exports = Bing

var request = require('superagent');
var co = require('co');
var yieldly = require('yieldly');
var parallel = require('co-parallel');

function Bing(opts) {
  if(!(this instanceof Bing)) return new Bing(opts)
  if(!opts.key) throw 'Please provide API Key';
  if(!opts.query) throw 'Please provide query';
  this.key = opts.key;
  this.query = opts.query;
  this.skip = opts.skip || 0;
  this.maxResults = opts.maxResults || 50;
  this.format = opts.format || 'JSON';
  this.baseUrl = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?";
  this.searchUrl = this.baseUrl + 'Query=%27' + encodeURI(this.query) + '%27&%24format=' + this.format + '&%24skip='
}

Bing.prototype.search = function(cb) {
  var self = this;
  var max = self.maxResults + self.skip;
  var requests = [];
  var responses = [];
  
  for(var i=self.skip; i < max; i += 50) {
    var trim = max - i;
    if(trim < 50) {
      requests.push(self.searchUrl+i+'&%24top='+trim)
    } else {
      requests.push(self.searchUrl+i)
    }
  }
  
  function *req(site) {
    var res = yield yieldly(get)(
                      site,
                      {user: self.key, key: self.key}
                    )
    return res
  }

  co(function* (){
    requests = requests.map(req)
    responses = yield parallel(requests, 10);
    cb.call(self, responses)
  }).catch(function(err){
      console.log(err.stack)
    })
};

function get(url, auth, fn) {
  request
    .get(url)
    .auth(auth.user, auth.key)
    .end(function(err, res){
      if(err) {
        var obj = {
          ok: false,
          error: err,
        };
      } else {
        var obj = {
          ok: true,
          status: res.status,
          text: res.text,
          body: res.body
        };
      }
      fn(null, obj);
    })
}