build: node_modules

clean:
	@rm -fr node_modules

node_modules: package.json
	@npm install

test: build
	@node --harmony example.js

.PHONY: clean test build